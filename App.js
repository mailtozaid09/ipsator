import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';

import HomeScreen from './src/screens/HomeScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const App = (props) => {

    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen 
                    name="Group Order Request" 
                    component={HomeScreen} 
                    options={{headerStyle: styles.headerStyle}}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({
    headerStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
        borderBottomWidth: 1,
    }
})


export default App