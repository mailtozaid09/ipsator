import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const LanguageSwitch = (props) => {

    const { isEnglish, onChangeValue, onSaveValue } = props; 

    return (
        <View style={styles.container} >
            <TouchableOpacity onPress={() => {onChangeValue(true); onSaveValue({name: 'language', value: 'English',});}} style={[styles.toggle, isEnglish ? {backgroundColor: Colors.LIGHT_ORANGE} : {backgroundColor: Colors.LIGHT_GRAY} ]} >
                {isEnglish ? <Image source={require('../../../assets/check.png')} style={styles.check} /> : null}
                <Text style={[styles.title, isEnglish ? {color: Colors.ORANGE} : null]} >ENGLISH</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {onChangeValue(false); onSaveValue({name: 'language', value: 'Hindi',});}} style={[styles.toggle, isEnglish ? {backgroundColor: Colors.LIGHT_GRAY} : {backgroundColor: Colors.LIGHT_ORANGE} ]} >
                {!isEnglish ? <Image source={require('../../../assets/check.png')} style={styles.check} /> : null}
                <Text style={[styles.title, !isEnglish ? {color: Colors.ORANGE} : null]} >HINDI</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        marginTop: 10,
        marginBottom: 20
    },
    title: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: '700',
        color: Colors.BLACK
    },
    toggle: {
        height: 40,
        padding: 10,
        paddingLeft: 20,
        paddingRight: 20,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderRadius: 23,
    },
    check: {
        height: 22,
        width: 22,
        marginRight: 10,
    }
})


export default LanguageSwitch