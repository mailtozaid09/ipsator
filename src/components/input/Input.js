import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const Input = (props) => {

    const { label, placeholder, onChange, error, numKeypad, msgInput, maxLen} = props; 

    return (
        <View>
            <Text style={styles.label} >{label}</Text>

            <TextInput 
                placeholder={placeholder}
                onChangeText={onChange}
                style={msgInput ? styles.msgInput : styles.input}
                keyboardType={numKeypad ? "numeric" : 'default'}
                multiline={msgInput ? true : false}
                textAlignVertical={msgInput ? "top" : null}
                maxLength={maxLen ? maxLen : null}
            />

            <Text style={styles.error} >{error ? error : null}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
       
    },
    input: {
        height: 50, 
        marginTop: 8,
        borderWidth: 1, 
        borderRadius: 4,
        paddingLeft: 15,
        fontSize: 16,
        fontWeight: '500',
        borderColor: Colors.GRAY,
    },
    msgInput: {
        height: 110, 
        marginTop: 8,
        borderWidth: 1, 
        borderRadius: 4,
        paddingLeft: 15,
        fontSize: 18,
        lineHeight: 20,
        fontWeight: '500',
        borderColor: Colors.GRAY,
    },
    label: {
        fontSize: 16,
        lineHeight: 18,
        marginTop: 20,
        fontWeight: '400',
        color: Colors.BLACK
    },
    error: {
        fontSize: 14,
        lineHeight: 18,
        marginTop: 2,
        color: Colors.RED
    },
})


export default Input