import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const Banner2 = (props) => {

    const {  } = props; 

    return (
        <View style={styles.mainContainer} >
         <View style={styles.container} >
            <View>
                <Image style={styles.icon} source={require('../../../assets/stopwatch.png')} />
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.title} >Submit as early as possible, longer processing time for large groups</Text>
            </View>
        </View>
        <View style={styles.container} >
            <View>
                <Image style={styles.icon} source={require('../../../assets/price.png')} />
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.title} >Best food options &  pricing informed after discussion</Text>
            </View>
        </View>
        <View style={styles.container} >
            <View>
                <Image style={styles.icon} source={require('../../../assets/pnr.png')} />
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.title} >PNR & full payment required to order, invoice provided after delivery</Text>
            </View>
        </View>
        </View>
       
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        padding: 15,
        backgroundColor: Colors.LIGHT_BLUE
    },
    container: {
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    textContainer: {
        flex: 1,
        marginLeft: 20,
    },
    icon: {
        height: 34,
        width: 34,
        resizeMode: 'contain'
    },
    title: {
        fontSize: 16,
        lineHeight: 26,
        color: Colors.DARK_GRAY,
        fontWeight: 'bold',
    },
})


export default Banner2