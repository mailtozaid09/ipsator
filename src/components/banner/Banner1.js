import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const Banner1 = (props) => {

    const {  } = props; 

    return (
        <View style={styles.container} >
            <View>
                <Image style={styles.icon} source={require('../../../assets/user.png')} />
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.title} >Travelling as a group?</Text>
                <Text style={styles.subTitle} >Share your details & we'll get back to you within next 24 hours with the best food deals!</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    textContainer: {
        flex: 1,
        marginLeft: 20,
    },
    icon: {
        height: 50,
        width: 50,
        resizeMode: 'contain'
    },
    title: {
        fontSize: 22,
        lineHeight: 32,
        color: Colors.BLACK,
        fontWeight: '700',
    },
    subTitle: {
        fontSize: 14,
        lineHeight: 28,
        color: Colors.BLACK
    },
})


export default Banner1