import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';
import Input from '../input/Input';

const {width} = Dimensions.get('window');

const Form = (props) => {

    const { onChange, errors, clearErrors, onSubmit } = props; 

    return (
        <View style={styles.container} >
            <Input
                label="Full Name"
                error={errors.name}
                maxLen={50}
                placeholder="Enter your full name"
                onChange={(text) => {onChange({name: 'name', value: text,}); clearErrors(); }}
            />

            <Input
                numKeypad
                label="Mobile Number"
                error={errors.mobile}
                maxLen={10}
                placeholder="Enter your mobile number"
                onChange={(text) => {onChange({name: 'mobile', value: text,}); clearErrors(); }}
            />

            <Input
                label="Email"
                error={errors.email}
                placeholder="Enter your email address"
                onChange={(text) => {onChange({name: 'email', value: text,}); clearErrors(); }}
            />

            <Input
                numKeypad
                label="Number of Passengers"
                placeholder="Number of Passengers"
                onChange={(text) => {onChange({name: 'passenger', value: text,}); clearErrors(); }}
            />

            <Input
                numKeypad
                label="PNR"
                maxLen={10}
                error={errors.pnr}
                placeholder="PNR"
                onChange={(text) => {onChange({name: 'pnr', value: text,}); clearErrors(); }}
            />

            <Input
                msgInput
                label="Message"
                placeholder="Tell us about type of food you are looking for (e.g. Thali, Birayani etc) and whether you need breakfast lunch or dinner."
                onChange={(text) => {onChange({name: 'message', value: text,}); clearErrors(); }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        paddingTop: 0,
        paddingBottom: 0,
    },
})


export default Form

