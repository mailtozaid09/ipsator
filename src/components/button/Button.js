import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const Button = (props) => {

    const { title, onPress } = props; 

    return (
        <TouchableOpacity activeOpacity={0.5} onPress={onPress} style={styles.button} >
            <Text style={styles.buttonText} >{title}</Text>
        </TouchableOpacity> 
    )
}

const styles = StyleSheet.create({
    button: {
        height: 40,
        margin: 15,
        marginBottom: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        backgroundColor: Colors.ORANGE, 
    },
    buttonText: {
        fontSize: 22,
        lineHeight: 24,
        fontWeight: '700',
        color: Colors.WHITE
    }
})


export default Button