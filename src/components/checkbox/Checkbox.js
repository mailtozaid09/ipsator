import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

import CheckBox from '@react-native-community/checkbox';


const {width} = Dimensions.get('window');

const Checkbox = (props) => {

    const { title, travelAgent, onSaveValue, onChangeValue } = props; 

    


    return (
        <View style={styles.container} >
            <CheckBox
                disabled={false}
                value={travelAgent}
                tintColors={{ true: Colors.BLUE}}
                onValueChange={(newValue) => {onSaveValue({name: 'travelAgent', value: newValue,}); onChangeValue(newValue)}}
            />

            <Text style={styles.title} >{title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 10,
    },
    title: {
        fontSize: 18,
        marginLeft: 10,
        lineHeight: 24,
        fontWeight: '400',
        color: Colors.BLACK
    }
})


export default Checkbox