import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Banner1 from '../components/banner/Banner1';
import Banner2 from '../components/banner/Banner2';
import Button from '../components/button/Button';
import Checbox from '../components/checkbox/Checkbox';
import Form from '../components/form/Form';
import LanguageSwitch from '../components/switch/LanguageSwitch';
import Colors from '../style/Colors';

const {width} = Dimensions.get('window');

const HomeScreen = (props) => {

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})
    const [travelAgent, setTravelAgent] = useState(false)
    const [isEnglish, setIsEnglish] = useState(true)
    
    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }

    const onSubmit = () => {
        console.log("-----SUBMIT----");
        console.log("-------- USER DETAILS --------");
        console.log(form);

        var isEmailValid = false;
        var isNameValid = false

        if(!form.name){
            console.log("Please enter a valid name");

            setErrors((prev) => {
                return {...prev, name: 'Please enter a valid name'}
            })
        }else{
            isNameValid = validateName(form.name)
        }

        if(!form.mobile || form.mobile.length < 10){
            console.log("Please enter a valid mobile number");

            setErrors((prev) => {
                return {...prev, mobile: 'Please enter a valid mobile number'}
            })
        }

        if(!form.pnr || form.pnr.length < 10){
            console.log("Please enter a valid PNR number");

            setErrors((prev) => {
                return {...prev, pnr: 'Please enter a valid PNR number'}
            })
        }
        
        if(!form.email){
            console.log("Please enter a valid email");

            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email'}
            })
        }else{
            isEmailValid = validateEmail(form.email)
        }

       
        if(form.name && form.mobile && form.email && form.pnr && isEmailValid && isNameValid ){
            alert('-------- USER DETAILS --------' + '\n \n' + 
                'Name :  ' + form.name + '\n' + 
                'Mobile :  ' + form.mobile + '\n' + 
                'PNR :  ' + form.pnr + '\n' +
                'Email :  ' + form.email + '\n \n' +
                JSON.stringify(form)
            )
        }
    }

    const validateEmail = (email) =>  {
        var validRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if (email.match(validRegex)) {
            return true;
        } else {
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email'}
            })
            return false;
        }
    }

    const validateName = (name) =>  {
        var validName = /^[A-Za-z]+$/;

        if (name.match(validName)) {
            return true;
        } else {
            setErrors((prev) => {
                return {...prev, name: 'Please enter a valid name'}
            })
            return false;
        }
    }

   

    return (
        <ScrollView>
            <View style={styles.container} >
                <Banner1 />
                <Banner2 />

                <Form
                    errors={errors}
                    clearErrors={() => setErrors({})}
                    onChange={onChange}
                    onSubmit={onSubmit} 
                /> 

                <LanguageSwitch
                    isEnglish={isEnglish}
                    onSaveValue={onChange}
                    onChangeValue={(val) => setIsEnglish(val)}
                />

                <Checbox
                    title="I am a travel agent"
                    travelAgent={travelAgent}
                    onSaveValue={onChange}
                    onChangeValue={(val) => setTravelAgent(val)}
                />

                <Button
                    title="SUBMIT"
                    onPress={onSubmit}
                />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.WHITE
    },
})


export default HomeScreen